#+options: toc:nil

My resume written with the excellent [[https://gitlab.com/Titan-C/org-cv][org-cv]].

For the =awesome-cv= compilation backend, some customization is taken
from
[[https://gitlab.com/zzamboni/vita/-/blob/master/texinput/awesome-cv.cls][Diego Zamboni's awesome-cv configuration]]!

* TODO
  - add export command to =C-c C-e= map.
** awesome-cv
  - Use =#+fontdir:= property to expand =\fonts[$1]= correctly instead
    of a symlink. *NOTE* relative paths do not work.
  - get rid of =resume/awesome-cv.cls= and modify
    =resume/Awesome-CV/awesome-cv.cls= directly, but push changes to
    my own repos.
